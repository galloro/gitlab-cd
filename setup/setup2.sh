#!/bin/bash
echo "Continuous Delivery on GCP demo setup"
echo

echo "Enter your GCP project ID:"
read yourproject
sed -i.bak "s/yourproject/$yourproject/g" cr-delivery-pipeline.yaml
sed -i.bak "s/yourproject/$yourproject/g" ../.gitlab-ci.yml
###
echo "Enter the region for your Cloud run services and Delivery Pipeline:"
read yourregion
sed -i.bak "s/yourregion/$yourregion/g" cr-delivery-pipeline.yaml
sed -i.bak "s/yourregion/$yourregion/g" ../.gitlab-ci.yml
###
echo "Enter the full path of your Artifact Registry repository:"
read yourrepo
sed -i.bak "s,yourrepo,$yourrepo,g" ../.gitlab-ci.yml
###
rm -rf cr-delivery-pipeline.yaml.bak
rm -rf ../.gitlab-ci.yml.bak
###
echo "Configuration successfully updated"
