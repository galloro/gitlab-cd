stages:
  - first-release
  - build
  - push
  - deploy-to-qa
  - promote-to-prod

variables:
  GITLAB_IMAGE: $CI_REGISTRY_IMAGE/cdongcp-app:$CI_COMMIT_SHORT_SHA
  GOOGLE_AR_REPO: yourrepo
  GOOGLE_PROJECT: yourproject
  GOOGLE_REGION: yourregion

# Image build for automatic pipeline running on merge request
image-build:
  image: docker:24.0.5
  stage: build
  services:
    - docker:24.0.5-dind
  rules:
    - if: $CI_PIPELINE_SOURCE == "web"
      when: never
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  script:
    - docker build -t $GITLAB_IMAGE cdongcp-app/
    - docker push $GITLAB_IMAGE
    - docker logout

# Image build for 1st manual release
1st-build:
  image: docker:24.0.5
  stage: first-release
  services:
    - docker:24.0.5-dind
  rules:
    - if: $CI_PIPELINE_SOURCE == "web"
  needs: []
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  script:
    - docker build -t $GITLAB_IMAGE cdongcp-app/
    - docker push $GITLAB_IMAGE
    - docker logout

include:
# Image push to Artifact Registry for automatic pipeline running on merge request
  - component: gitlab.com/google-gitlab-components/artifact-registry/upload-artifact-registry@0.1.1
    inputs:
      stage: push
      source: $GITLAB_IMAGE
      target: $GOOGLE_AR_REPO/cdongcp-app:$CI_COMMIT_SHORT_SHA

# Cloud Deploy release creation for automatic pipeline running on merge request
  - component: gitlab.com/google-gitlab-components/cloud-deploy/create-cloud-deploy-release@0.1.1 
    inputs: 
      stage: deploy-to-qa
      project_id: $GOOGLE_PROJECT
      name: cdongcp-$CI_COMMIT_SHORT_SHA
      delivery_pipeline: cd-on-gcp-pipeline
      region: $GOOGLE_REGION
      images: cdongcp-app=$GOOGLE_AR_REPO/cdongcp-app:$CI_COMMIT_SHORT_SHA

# Cloud Deploy release promotion for automatic pipeline running on merge request
  - component:  gitlab.com/google-gitlab-components/cloud-sdk/run-gcloud@main
    inputs:
      stage: promote-to-prod
      project_id: $GOOGLE_PROJECT
      commands: |
          MOST_RECENT_RELEASE=$(gcloud deploy releases list --delivery-pipeline cd-on-gcp-pipeline --region $GOOGLE_REGION --format="value(name)" --limit 1)
          gcloud deploy releases promote --delivery-pipeline cd-on-gcp-pipeline --release $MOST_RECENT_RELEASE --region $GOOGLE_REGION
 

# Image push to Artifact Registry for 1st manual release
  - component: gitlab.com/google-gitlab-components/artifact-registry/upload-artifact-registry@0.1.1 
    inputs:
      as: first-ar-push
      stage: first-release
      source: $GITLAB_IMAGE
      target: $GOOGLE_AR_REPO/cdongcp-app:$CI_COMMIT_SHORT_SHA

# Cloud Deploy release creation for 1st manual release
  - component: gitlab.com/google-gitlab-components/cloud-deploy/create-cloud-deploy-release@0.1.1 
    inputs: 
      as: create-1st-release
      stage: first-release
      project_id: $GOOGLE_PROJECT
      name: cdongcp-$CI_COMMIT_SHORT_SHA
      delivery_pipeline: cd-on-gcp-pipeline
      region: $GOOGLE_REGION
      images: cdongcp-app=$GOOGLE_AR_REPO/cdongcp-app:$CI_COMMIT_SHORT_SHA

# Cloud Deploy release promotion and approval for 1st manual release
  - component:  gitlab.com/google-gitlab-components/cloud-sdk/run-gcloud@main
    inputs:
      as: promote-1st-release
      stage: first-release
      project_id: $GOOGLE_PROJECT
      commands: |
          MOST_RECENT_RELEASE=$(gcloud deploy releases list --delivery-pipeline cd-on-gcp-pipeline --region $GOOGLE_REGION --format="value(name)" --limit 1)
          gcloud deploy releases promote --delivery-pipeline cd-on-gcp-pipeline --release $MOST_RECENT_RELEASE --region $GOOGLE_REGION --starting-phase-id=stable
          MOST_RECENT_ROLLOUT=$(gcloud deploy rollouts list --release $MOST_RECENT_RELEASE --format="value(name)" --limit 1)
          gcloud deploy rollouts approve $MOST_RECENT_ROLLOUT --delivery-pipeline cd-on-gcp-pipeline --region $GOOGLE_REGION --release $MOST_RECENT_RELEASE --quiet

# Allow unauthenticated requests to Cloud Run services after 1st manual release
  - component:  gitlab.com/google-gitlab-components/cloud-sdk/run-gcloud@main
    inputs:
      as: set-cr-authentication
      stage: first-release
      project_id: $GOOGLE_PROJECT
      commands: |
          gcloud run services add-iam-policy-binding cdongcp-app-qa --member="allUsers" --role="roles/run.invoker" --region $GOOGLE_REGION
          gcloud run services add-iam-policy-binding cdongcp-app-prod --member="allUsers" --role="roles/run.invoker" --region $GOOGLE_REGION

# Rules for automated pipeline jobs
upload-artifact-registry:
  rules:
    - if: $CI_PIPELINE_SOURCE == "web"
      when: never
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
create-cloud-deploy-release:
  rules:
    - if: $CI_PIPELINE_SOURCE == "web"
      when: never
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
run-gcloud:
  rules:
    - if: $CI_PIPELINE_SOURCE == "web"
      when: never
    - if: $CI_COMMIT_MESSAGE =~ /^Revert/
      when: never
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

# Rules and dependencies for 1st manual release jobs
first-ar-push:
  rules:
    - if: $CI_PIPELINE_SOURCE == "web"
  needs: ["1st-build"]
create-1st-release:
  rules:
    - if: $CI_PIPELINE_SOURCE == "web"
  needs: ["first-ar-push"]
promote-1st-release:
  rules:
    - if: $CI_PIPELINE_SOURCE == "web"
      when: delayed
      start_in: 60 seconds
  needs: ["create-1st-release"]
set-cr-authentication:
  rules:
    - if: $CI_PIPELINE_SOURCE == "web"
  needs: ["promote-1st-release"]