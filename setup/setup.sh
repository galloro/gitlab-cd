#!/bin/bash
# This version of the script works only with GNU sed
# If using a mac, please run setup2.sh
echo "Continuous Delivery on GCP demo setup"
echo
###
echo "Enter your GCP project ID:"
read yourproject
sed -i "s/yourproject/$yourproject/g" cr-delivery-pipeline.yaml
sed -i "s/yourproject/$yourproject/g" ../.gitlab-ci.yml
###
echo "Enter the region for your Cloud run services and Delivery Pipeline:"
read yourregion
sed -i "s/yourregion/$yourregion/g" cr-delivery-pipeline.yaml
sed -i "s/yourregion/$yourregion/g" ../.gitlab-ci.yml
###
echo "Enter the full path of your Artifact Registry repository:"
read yourrepo
sed -i "s,yourrepo,$yourrepo,g" ../.gitlab-ci.yml
###
echo "Configuration successfully updated"